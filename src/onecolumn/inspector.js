/**
 * Inspector controls
 */
const { __ } = wp.i18n;
const { Component, Fragment } = wp.element;
const { InspectorControls, MediaUpload, ColorPalette, PanelColorSettings } = wp.editor;
const { PanelBody, TextControl, Button, ButtonGroup } = wp.components;
// const { withState } = wp.compose;

import ButtonPalette from '../../components/button-dropdown';
import options from '../../components/bgimage-options';

export default class Inspector extends Component {
	render() {
		const { attributes, setAttributes } = this.props;
		const { backgroundColor, backgroundType, solidColor, mediaID, mediaURL, diamondID, diamondURL, buttonStyle } = attributes;
		// Event(s)
		const onChangeBackgroundType = newType => {
			setAttributes( {
				backgroundType: newType,
			} );
		};
		const onChangeSolidColor = newColor => {
			setAttributes( { solidColor: newColor } );
		};
		const onSelectDiamondImage = diamond => {
			setAttributes( {
				diamondID: diamond.id,
				diamondURL: diamond.url,
			} );
		};
		const onRemoveDiamondImage = () => {
			setAttributes( {
				diamondID: undefined,
				diamondURL: undefined,
			} );
		};
		const onSelectImage = media => {
			setAttributes( {
				mediaID: media.id,
				mediaURL: media.url,
			} );
		};
		const onRemoveImage = () => {
			setAttributes( {
				mediaID: undefined,
				mediaURL: undefined,
			} );
		};
		const changeBackgroundColor = value => {
			setAttributes( { backgroundColor: value } );
		};

		const onChangeButtonStyle = buttonStyle => {
			setAttributes( { buttonStyle } );
		};

		// Controls
		const colorControl = (
			<ColorPalette value={ solidColor } onChange={ onChangeSolidColor } />
		);

		return (
			<InspectorControls key="inspector">
				<PanelBody title={ __( 'Background Settings' ) } initialOpen={ true }>
					<ButtonGroup
						aria-label={ __( 'Background Type' ) }
						className="block-background__type"
					>
						{ options.backgroundType.map( type => {
							return (
								<Button
									key={ type.label }
									isLarge
									isPrimary={ backgroundType === type.value }
									aria-pressed={ backgroundType === type.value }
									onClick={ () => onChangeBackgroundType( type.value ) }
								>
									{ type.label }
								</Button>
							);
						} ) }
					</ButtonGroup>

					{ 'color' === backgroundType && colorControl }

					{ 'diamond' === backgroundType && colorControl && (
						<MediaUpload
							key="mediaupload"
							onSelect={ onSelectDiamondImage }
							type="image"
							value={ diamondID }
							render={ ( { open } ) => (
								<Fragment>
									<Button className="button-link" onClick={ open }>
										{ ! diamondID ? (
											__( 'Set Diamond Pattern' )
										) : (
											<img src={ diamondURL } alt="" />
										) }
									</Button>
								</Fragment>
							) }
						/>
					) }
					{ // Actions for background image selected
						'diamond' === backgroundType &&
              diamondID && (
							// eslint-disable-next-line react/jsx-indent
							<Fragment>
								<p className="editor-post-featured-image__howto">
									{ __( 'Click the image to edit or update' ) }
								</p>
								<Button
									className="button-link"
									style={ { marginBottom: '20px' } }
									onClick={ onRemoveDiamondImage }
								>
									{ __( 'Remove Diamond Texture' ) }
								</Button>
							</Fragment>
						) }

					{ 'image' === backgroundType && (
						<MediaUpload
							key="mediaupload"
							onSelect={ onSelectImage }
							type="image"
							value={ mediaID }
							render={ ( { open } ) => (
								<Fragment>
									<Button className="button-link" onClick={ open }>
										{ ! mediaID ? (
											__( 'Set background image' )
										) : (
											<img src={ mediaURL } alt="" />
										) }
									</Button>
								</Fragment>
							) }
						/>
					) }

					{ // Actions for background image selected
						'image' === backgroundType &&
              mediaID && (
							// eslint-disable-next-line react/jsx-indent
							<Fragment>
								<p className="editor-post-featured-image__howto">
									{ __( 'Click the image to edit or update' ) }
								</p>
								<Button
									className="button-link"
									style={ { marginBottom: '20px' } }
									onClick={ onRemoveImage }
								>
									{ __( 'Remove background image' ) }
								</Button>
							</Fragment>
						) }
				</PanelBody>
				<PanelBody title={ __( 'Background Settings' ) } >

					<Fragment>
						<p>{ __( 'Background Color' ) }</p>

						<ColorPalette
							label={ 'Background Color' }
							value={ backgroundColor }
							onChange={ changeBackgroundColor }
						/>
					</Fragment>
				</PanelBody>
				<PanelColorSettings
					title={ __( 'Color Settings', 'gap-golf-blocks' ) }
					colorSettings={ [
						{
							value: attributes.eyebrowColor,
							onChange: ( eyebrowColor ) => setAttributes( { eyebrowColor } ),
							label: __( 'Eyebrow Color', 'gap-golf-blocks' ),
						},
						{
							value: attributes.headingColor,
							onChange: ( headingColor ) => setAttributes( { headingColor } ),
							label: __( 'Heading Color', 'gap-golf-blocks' ),
						},
						{
							value: attributes.textColor,
							onChange: ( textColor ) => setAttributes( { textColor } ),
							label: __( 'Text Color', 'gap-golf-blocks' ),
						},
					] }
				>
				</PanelColorSettings>
				<PanelBody initialOpen={ false } title={ __( 'Button Settings', 'gap-golf-blocks' ) }>

					<Fragment>
						<ButtonPalette
							label={ __( 'Select Button Style:', 'gap-golf-blocks' ) }
							value={ buttonStyle }
							// eslint-disable-next-line no-shadow
							onChange={ onChangeButtonStyle }

						/>
						<TextControl
							label={ __( 'Button Text', 'gap-golf-blocks' ) }
							value={ attributes.buttonText }
							onChange={ ( buttonText ) => setAttributes( { buttonText } ) }
						/>
						<TextControl
							label={ __( 'Button URL', 'gap-golf-blocks' ) }
							value={ attributes.buttonURL }
							onChange={ ( buttonURL ) => setAttributes( { buttonURL } ) }
						/>
					</Fragment>

				</PanelBody>

			</InspectorControls>
		);
	}
}
