/**
 * BLOCK: One Column Block
 */

/**
 * External dependencies
 */
//import classnames from 'classnames';

//  Import CSS.
import './styles.scss';
import './editor.scss';
import Inspector from './inspector';
import classnames from 'classnames';

const { __ } = wp.i18n;
const { Fragment } = wp.element;
const { registerBlockType } = wp.blocks;
const { RichText, BlockControls, AlignmentToolbar } = wp.editor;
// import ButtonPalette from '../../components/button-dropdown';
// import DiamondCheckboxControl from '../../components/diamond-checkbox';

registerBlockType( 'gap-golf-blocks/onecolumn', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'One Column ' ), // Block title.
	icon: 'format-aside', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'gap-golf-blocks', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'GAP Block' ),
		__( 'One Column' ),
		__( 'Layout' ),
	],
	attributes: {
		eyebrow: {
			source: 'children',
			selector: '.one-up__eyebrow',
		},
		heading: {
			source: 'children',
			selector: '.one-up__heading',
		},
		text: {
			source: 'children',
			selector: '.one-up__text',
		},
		alignment: {
			type: 'string',
			default: 'center',
		},
		position: {
			type: 'string',
			default: 'center',
		},
		width: {
			type: 'number',
			default: 500,
		},
		eyebrowColor: {
			type: 'string',
		},
		headingColor: {
			type: 'string',
		},
		textColor: {
			type: 'string',
		},
		showButton: {
			type: 'bool',
			default: true,
		},
		buttonStyle: {
			type: 'string',
			default: 'fancy-button green',
		},
		buttonText: {
			type: 'string',
			default: __( 'Click Here', 'gap-golf-blocks' ),
		},
		buttonURL: {
			type: 'string',
		},
	},

	edit: function( props ) {
		const { attributes, setAttributes } = props;

		const styles = {
			styleVar1: { backgroundColor: attributes.backgroundColor, backgroundImage: `url(${ attributes.mediaURL })`, textAlign: attributes.alignment },
			//styleVar2: { background: URL() }, - will need for this on the Diamond Selection
		};

		const oneUpStyle = {
			textAlign: attributes.alignment,
		};

		return (
			<Fragment>
				<Inspector { ...props } />
				<BlockControls key="controls">
					<AlignmentToolbar
						value={ attributes.alignment }
						onChange={ ( alignment ) => setAttributes( { alignment } ) }
					/>
				</BlockControls>
				<div style={ styles.styleVar1 } >
					<div className="inner" style={ oneUpStyle }>
						<RichText
							value={ attributes.eyebrow }
							onChange={ ( eyebrow ) => setAttributes( { eyebrow } ) }
							tagName="div"
							placeholder={ __( 'Eyebrow Text', 'gap-golf-blocks' ) }
							formattingControls={ [] }
							keepPlaceholderOnFocus={ true }
							style={ { color: attributes.eyebrowColor } }
							className="eyebrow"
						/>
						<RichText
							value={ attributes.heading }
							onChange={ ( heading ) => setAttributes( { heading } ) }
							tagName="h1"
							placeholder={ __( 'Heading Text', 'gap-golf-blocks' ) }
							formattingControls={ [] }
							keepPlaceholderOnFocus={ true }
							style={ { color: attributes.headingColor } }
							className=""
						/>
						<RichText
							value={ attributes.text }
							onChange={ ( text ) => setAttributes( { text } ) }
							tagName="p"
							placeholder={ __( 'Content Text', 'gap-golf-blocks' ) }
							formattingControls={ [] }
							keepPlaceholderOnFocus={ true }
							style={ { color: attributes.textColor } }
						/>
						<a className={ attributes.buttonStyle } href={ attributes.URL }><span>{ attributes.buttonText }</span></a>
					</div>
				</div>
			</Fragment>
		);
	},
	save: function( props ) {
		const { attributes } = props;

		const styles = {
			styleVar1: { backgroundColor: attributes.backgroundColor, backgroundImage: `url(${ attributes.mediaURL })`, textAlign: attributes.alignment },
			//styleVar2: { background: URL() },
		};

		const twoUpStyle = {
			textAlign: attributes.alignment,
		};

		return (
			<div className="wp-block-gap-blocks-one-up">
				<div className="hero">
					<div className="hero-item" style={ styles.styleVar1 }>
						<div className="hero_inner" style={ twoUpStyle }>
							<div className="hero_copy_center">
								<RichText.Content
									tagName="div"
									style={ { color: attributes.eyebrowColor } }
									value={ attributes.eyebrow }
									className="eyebrow"
								/>
								<RichText.Content
									tagName="h1"
									style={ { color: attributes.headingColor } }
									value={ attributes.heading }
									className=""
								/>
								<RichText.Content
									tagName="p"
									style={ { color: attributes.textColor } }
									value={ attributes.text }
									className=""
								/>
								<a className={ attributes.buttonStyle } href={ attributes.buttonURL }><span>{ attributes.buttonText }</span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	},
} );
