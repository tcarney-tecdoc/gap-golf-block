/**
 * Options for radio/select
 */

const options = {};

options.backgroundType = [
	{ label: 'Diamond Pattern', value: 'diamond' },
	{ label: 'Image', value: 'image' },
];

export default options;
