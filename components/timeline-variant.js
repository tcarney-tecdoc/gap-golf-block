const { CheckboxControl } = wp.components;
const { withState } = wp.compose;

const TimelineCheckboxControl = withState( {
    isChecked: true,
} )( ( { isChecked, setState } ) => ( 
    <CheckboxControl
        heading="Timeline Variant"
        label="On the Legacy Page"
        help="Is this column part of a Legacy Page?"
        checked={ isChecked }
        onChange={ ( isChecked ) => { setState( { isChecked } ) } }
    />
) );
export default TimelineCheckboxControl;