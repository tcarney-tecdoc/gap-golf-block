const { __ } = wp.i18n;
const { SelectControl } = wp.components;
const { withState } = wp.compose;

const ButtonPalette = withState( {
	buttonStyle: 'fancy-button green',
} )( ( { buttonStyle, setState } ) => (
	<SelectControl
		label={ __( 'Select Button Style:' ) }
		value={ buttonStyle }
		onChange={ ( buttonStyle )=> setState( { buttonStyle } ) }
		options={ [
			{ value: 'fancy-button none', label: __( 'None', 'gap-golf-blocks' ) },
			{ value: 'fancy-button green', label: __( 'Green', 'gap-golf-blocks' ) },
			{ value: 'fancy-button ltgreen', label: __( 'Light Green', 'gap-golf-blocks' ) },
			{ value: 'fancy-button gold', label: __( 'Gold', 'gap-golf-blocks' ) },
			{ value: 'fancy-button yellow', label: __( 'Yellow', 'gap-golf-blocks' ) },
			{ value: 'fancy-button orange', label: __( 'Orange', 'gap-golf-blocks' ) },
			{ value: 'fancy-button white', label: __( 'White', 'gap-golf-blocks' ) },
			{ value: 'fancy-button greenborder', label: __( 'Green Bordered', 'gap-golf-blocks' ) },
			{ value: 'fancy-button ltgreenborder', label: __( 'Light Green Bordered', 'gap-golf-blocks' ) },
			{ value: 'fancy-button goldborder', label: __( 'Gold Border', 'gap-golf-blocks' ) },
			{ value: 'fancy-button yellowborder', label: __( 'Yellow Bordered', 'gap-golf-blocks' ) },
			{ value: 'fancy-button orangeborder', label: __( 'Orange Border', 'gap-golf-blocks' ) },
			{ value: 'fancy-button whiteborder', label: __( 'White Bordered', 'gap-golf-blocks' ) },
		] }
	/>
) );
export default ButtonPalette;

