/**
 * Attributes Object
 */
const backgroundSettings = {
	backgroundType: {
		type: 'string',
	},
	solidColor: {
		type: 'string',
	},
	mediaID: {
		type: 'number',
	},
	mediaURL: {
		type: 'string',
	},
	diamondID: {
		type: 'number',
	},
	diamondURL: {
		type: 'string',
	},
};

export default backgroundSettings;
