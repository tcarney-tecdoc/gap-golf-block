const { CheckboxControl } = wp.components;
const { withState } = wp.compose;

const DiamondCheckboxControl = withState( {
	isChecked: true,
} )( ( { isChecked, setState } ) => (
	<CheckboxControl
		heading="Diamond Background"
		label="Select Diamond Background"
		help="Does the Diamond Background fit your layout?"
		checked={ isChecked }
		onChange={ ( isCheckedChanged ) => {
			setState( { isCheckedChanged } );
			// eslint-disable-next-line indent
        } }
	/>
) );
export default DiamondCheckboxControl;
