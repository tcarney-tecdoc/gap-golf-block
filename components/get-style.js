/**
 * Internal Dependencies
 */

/**
 * Generate Style Object for background
 */

function getStyle( attributes ) {
	// Block Settings
	const {
		backgroundType,
		solidColor,
		mediaURL,
		diamondURL,
	} = attributes;

	// Style
	let style = {};

	switch ( backgroundType ) {
		case 'color':
			style = { background: solidColor };
			break;
		case 'diamond':
			style = { background: `${ solidColor } url( ${ diamondURL } )` };
			break;
		case 'image':
			// TOOD: Refactor
			style = {
				background: `url( ${ mediaURL } )`,
				backgroundSize: 'cover',
			};
			break;
	}
	return style;
}

export default getStyle;
