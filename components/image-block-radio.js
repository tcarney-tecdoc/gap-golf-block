const { RadioControl } = wp.components;
const { withState } = wp.compose;

const ImageBlockRadioControl = withState( {
	option: 'isolated-image',
} )( ( { option, setState } ) => (
	<RadioControl
		label="Image Block Settings"
		help="Image Block Type needed on the block"
		selected={ option }
		options={ [
			{ label: 'Isolated Image', value: 'isolated-image' },
			{ label: 'Masked Image (Bleed)', value: 'masked-image' },
			{ label: 'Timeline Masked Image', value: 'timeline-masked-image' },
		] }
		onChange={ ( optionChange ) => {
			setState( { optionChange } );
		} }
	/>
) );
export default ImageBlockRadioControl;
