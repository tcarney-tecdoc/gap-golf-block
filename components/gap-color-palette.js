const { ColorPalette } = wp.components;
const { withState } = wp.compose;

const GapColorPalette = withState( {
	color: '#f00',
} )( ( { color, setState } ) => {
	const colors = [
		{ name: 'red', color: '#f00' },
		{ name: 'white', color: '#fff' },
		{ name: 'blue', color: '#00f' },
	];

	return (
		<ColorPalette
			colors={ colors }
			value={ color }
			onChange={ ( colorChange ) => setState( { colorChange } ) }
		/>
	);
} );
export default GapColorPalette;
