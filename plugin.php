<?php
/**
 * Plugin Name: GAP Golf Blocks
 * Plugin URI: https://github.com/ahmadawais/create-guten-block/
 * Description: GAP Blocks is a Gutenberg plugin created via create-guten-block.  It is based on the GAP website.
 * Author: 2one5 Creative
 * Author URI: https://2one5.com/
 * Version: 1.0.0
 * License: GPL2+
 * License URI: https://www.gnu.org/licenses/gpl-2.0.txt
 *
 * @package gap-golf
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

define( 'GAP_GOLF_BLOCKS_VERSION', '1.0.0' );

/**
 * Add a redirection check on activation.
 */
function gap_golf_blocks_activate() {
	add_option( 'gap_blocks_do_activation_redirect', true );
}
register_activation_hook( __FILE__, 'gap_golf_blocks_activate' );


/**
 * Redirect to the Gute Blocks Help page.
 */
function gap_golf_blocks_redirect() {
	if ( get_option( 'gap_golf_blocks_do_activation_redirect', false ) ) {
		delete_option( 'gap_golf_blocks_do_activation_redirect' );
		if ( ! isset( $_GET['activate-multi'] ) ) {
			wp_redirect( 'admin.php?page=gap-golf-blocks' );
		}
	}
}
add_action( 'admin_init', 'gap_golf_blocks_redirect' );

// Add custom block category.
add_filter(
	'block_categories',
	function( $categories, $post ) {
		return array_merge(
			$categories,
			array(
				array(
					'slug'  => 'gap-golf-blocks',
					'title' => __( 'GAP Blocks', 'gap-golf-blocks' ),
				),
			)
		);
	},
	10,
	2
);

/**
 * Block Initializer.
 */
require_once plugin_dir_path( __FILE__ ) . 'src/init.php';
// require_once plugin_dir_path( __FILE__ ) . 'admin/welcome.php';
// require_once plugin_dir_path( __FILE__ ) . 'admin/class-gute-blocks-notice.php';
